import { Navigation } from 'react-native-navigation';
import App from './App';
import Login from './src/telas/Login';

Navigation.registerComponent('Login', () => Login);
Navigation.registerComponent('App', () => App);

/*Navigation.startTabBasedApp({
  tabs: [
    {
      label: 'Aluno',
      screen: 'App',
      icon: require('./src/resources/cliente50x50.png'),
      title: 'PrjTCC'
    },
    {
      label: 'Profissionais',
      screen: 'App',
      icon: require('./src/resources/profissional50x50.png'),
      title: 'PrjTCC'
    }
  ],
  appStyle: {
    orientation: 'portrait',
  }
});*/


Navigation.startSingleScreenApp({
  screen: {
    screen: 'Login',
    title: 'Login',
  },
});