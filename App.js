/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  FlatList,
} from 'react-native';

import Anuncio from './src/componentes/Anuncio';

type Props = {};
export default class App extends Component<Props> {
  static navigatorStyle = {
    navBarTitleTextCentered: true,
    navBarTransparent: true,
    topBarElevationShadowEnabled: false,
  };

  constructor() {
    super();

    this.state = {
      listaAnuncios: [],
    };
  }

  componentDidMount() {
    this.setState({
      listaAnuncios: [
        {
          id: 1,
          nome: 'Profissinal 1',
          valor: '23,00',
          nota: 1,
          endereco: 'Rua Teste1',
          titulo: 'Aula de Tenis',
          tipoAula: 'Individual',
          horaAula: '20:00',
          semana: [
            {
              dia: 'D',
              selecionado: false,
            },
            {
              dia: 'S',
              selecionado: false,
            },
            {
              dia: 'T',
              selecionado: false,
            },
            {
              dia: 'Q',
              selecionado: true,
            },
            {
              dia: 'Q',
              selecionado: false,
            },
            {
              dia: 'S',
              selecionado: true,
            },
            {
              dia: 'S',
              selecionado: false,
            },
          ],
        },
        {
          id: 2,
          nome: 'Profissinal 2',
          valor: '23,00',
          nota: 2,
          endereco: 'Rua teste2',
          titulo: 'Aula de basquete',
          tipoAula: 'Em grupo',
          horaAula: '14:00',
          semana: [
            {
              dia: 'D',
              selecionado: false,
            },
            {
              dia: 'S',
              selecionado: false,
            },
            {
              dia: 'T',
              selecionado: false,
            },
            {
              dia: 'Q',
              selecionado: true,
            },
            {
              dia: 'Q',
              selecionado: false,
            },
            {
              dia: 'S',
              selecionado: true,
            },
            {
              dia: 'S',
              selecionado: false,
            },
          ],
        },
        {
          id: 3,
          nome: 'Profissinal 3',
          valor: '23,00',
          nota: 3,
          endereco: 'Rua Teste3',
          titulo: 'Aula de Personal',
          tipoAula: 'Individual',
          horaAula: '13:00',
          semana: [
            {
              dia: 'D',
              selecionado: false,
            },
            {
              dia: 'S',
              selecionado: false,
            },
            {
              dia: 'T',
              selecionado: false,
            },
            {
              dia: 'Q',
              selecionado: true,
            },
            {
              dia: 'Q',
              selecionado: false,
            },
            {
              dia: 'S',
              selecionado: true,
            },
            {
              dia: 'S',
              selecionado: false,
            },
          ],
        },
      ],
    });
  }

  _renderizaAnuncios = ({item}) => <Anuncio anuncio={item}/>;

  render() {
    return (
      <FlatList
        style={{flex: 1, marginTop: 60}}
        contentContainerStyle={styles.container}
        data={this.state.listaAnuncios}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderizaAnuncios}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});
