/**
 * @flow
 */

import React, { Component } from 'react';
import {
  View,
  TextInput,
  Button,
  Text,
} from 'react-native';

import { Navigation } from 'react-native-navigation';

import styles from '../estilos/loginEstilos';

type Props = {};
export default class Login extends Component<Props> {
  static navigatorStyle = {
    navBarTitleTextCentered: true,
    navBarTransparent: true,
    topBarElevationShadowEnabled: false,
  };

  state = {
    usuario: '',
    senha: '',
    msgErro: '',
  };

  login = () => {
    const {usuario, senha} = this.state;
    if (usuario === '' || senha === '') {
      this.setState({msgErro: 'Preencha todos os campos corretamente!'});
    } else {
      Navigation.startTabBasedApp({
        tabs: [
          {
            label: 'Aluno',
            screen: 'App',
            icon: require('../resources/cliente50x50.png'),
            title: 'PrjTCC',
          },
          {
            label: 'Profissionais',
            screen: 'App',
            icon: require('../resources/profissional50x50.png'),
            title: 'PrjTCC',
          },
        ],
        appStyle: {
          orientation: 'portrait',
        },
      });
    }
  };

  render() {
    const {usuario, senha, msgErro} = this.state;
    return (
      <View style={styles.container}>
        <TextInput
          value={usuario}
          onChangeText={(usuario) => this.setState({usuario, msgErro: ''})}
          placeholder="Usuario"
        />
        <TextInput
          value={senha}
          onChangeText={(senha) => this.setState({senha, msgErro: ''})}
          placeholder="Senha"
          secureTextEntry={true}
        />
        <Text style={{color: 'red', margin: 10,}}>{msgErro}</Text>
        <Button title="Login" onPress={this.login}/>
      </View>
    );
  }
}


