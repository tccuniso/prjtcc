import { Dimensions, StyleSheet } from 'react-native';
const width = Dimensions.get('screen').width;

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: width * 0.1,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: '#F5FCFF',
  },
});