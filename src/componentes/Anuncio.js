/**
 * @flow
 */

import React from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
} from 'react-native';

import DiasSelecionados from './DiasSelecionados';

const width = Dimensions.get('window').width;

export default Anuncio = ({anuncio}) => {
  const {nome, valor, nota, endereco, titulo, tipoAula, horaAula, semana} = anuncio;

  let imgNota;
  switch (nota) {
    case 0: {
      imgNota = require('../resources/0-estrelas.png');
      break;
    }
    case 1: {
      imgNota = require('../resources/1-estrela.png');
      break;
    }
    case 2: {
      imgNota = require('../resources/2-estrelas.png');
      break;
    }
    case 3: {
      imgNota = require('../resources/3-estrelas.png');
      break;
    }
    case 4: {
      imgNota = require('../resources/4-estrelas.png');
      break;
    }
    case 5: {
      imgNota = require('../resources/5-estrelas.png');
      break;
    }
  }

  return (
    <View style={styles.container}>
      <View style={styles.conteudoPrincipal}>
        <Text style={styles.titulo}>{titulo}</Text>
        <View style={styles.conteudoSecundario}>
          <Text style={styles.textoPadrao}>{tipoAula}</Text>
          <Text style={styles.textoPadrao}>{horaAula}</Text>
        </View>
        <Text style={[styles.endereco, styles.textoPadrao]}>{endereco}</Text>
        <View style={styles.conteudoSecundario}>
          <DiasSelecionados semana={semana}/>
          <Text style={styles.textoPadrao}>R$ {valor}</Text>
        </View>
        <View style={styles.conteudoSecundario}>
          <TouchableOpacity>
            <Text style={styles.verMais}>ver mais</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text style={styles.agendar}>agendar</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.rodapeAnuncio}>
        <Text style={styles.textoPadrao}>{nome}</Text>
        <Image style={styles.imgEstrelas} source={imgNota}/>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: width * 0.9,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    backgroundColor: '#F5FCFE',
    borderWidth: 1,
    marginBottom: 15,
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderColor: '#DDDDDD',
  },
  titulo: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000000',
  },
  conteudoSecundario: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 15,
    marginBottom: 15,
  },
  conteudoPrincipal: {
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    padding: 20,
    paddingTop: 15,
  },
  imgEstrelas: {
    width: 120,
    height: 20,
  },
  rodapeAnuncio: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopWidth: 1,
    borderColor: '#DDDDDD',
    padding: 10,
  },
  endereco: {
    textAlign: 'center',
  },
  agendar: {
    backgroundColor: '#EEEEEE',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 10,
  },
  verMais: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'blue',
  },
  textoPadrao: {
    fontSize: 14,
    color: '#333333',
  },
});