/**
 * @flow
 */

import React from 'react';
import {
  StyleSheet,
  Text,
  FlatList,
} from 'react-native';

const renderizaDia = ({item}) => <Text style={
  [
    {
      color: item.selecionado ? 'blue' : 'red',
      fontWeight: item.selecionado ? 'bold' : '100',
    },
    styles.diaSemana,
  ]
}>{item.dia}</Text>;

export default DiasSelecionados = ({semana}) => {
  return (
    <FlatList
      style={styles.container}
      data={semana}
      keyExtractor={(item, index) => index.toString()}
      renderItem={renderizaDia}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  diaSemana: {
    marginRight: 5,
  },
});